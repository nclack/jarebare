use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::{
    Display,
};
use colored::*;

#[derive(Serialize,Deserialize,Debug,Clone)]
pub enum Action {
    Attack(String),
    Defend(String)
}

#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Ability {
    pub name: String,
    pub action : Option<Action>
}

#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Card {
    pub tier: u8,
    pub name: String,
    pub class: String,
    pub comment: String,
    pub health: u32,
    pub defense: u32,
    pub abilities: [Ability;6]
}

impl Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let name=match &self.tier {
            1 => self.name.normal(),
            2 => self.name.yellow(),
            3 => self.name.purple(),
            _ => self.name.bright_blue()
        }.bold();        
        writeln!(f,"{}",name)?;
        writeln!(f,"❤️ {} ⛨ {}",self.health, self.defense)?;
        writeln!(f,"{}",self.comment.italic())?;
        let width=self.abilities.iter()
            .map(|a|a.name.len())
            .max()
            .unwrap_or(1);
        for (ability,die) in self.abilities.iter().zip("⚀⚁⚂⚃⚄⚅".chars()) {
            let act=match &ability.action {
                    Some(Action::Attack(d)) => d.red(),
                    Some(Action::Defend(d)) => d.green(),
                    None => "".normal()
                };            
            writeln!(f,"{} {:width$}{}",die,ability.name,act,width=width+2)?;
        }
        Ok(())
    }
}