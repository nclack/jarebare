use jarebare::Card;
use clap::{App,Arg,SubCommand};
use std::{
	io::BufReader,
	fs::File,
	path::Path,
	ffi::OsStr,
	error::Error
};
use rand::thread_rng;
use rand::seq::SliceRandom;

fn read_cards<P:AsRef<Path>>(filename:P)->Result<Vec<Card>,Box<Error>> {
	let filename=filename.as_ref();
	let file=File::open(filename)?;
	let reader=BufReader::new(file);
	Ok(match filename.extension().and_then(OsStr::to_str) {
		Some("json") => serde_json::from_reader(reader)?,
		Some("yaml") => serde_yaml::from_reader(reader)?,
		// By default just try json
		Some(_) => serde_json::from_reader(reader)?,
		None => serde_json::from_reader(reader)?,
	})
}

fn main() -> Result<(),Box<std::error::Error>> {
	let matches=App::new("jarebare")
		.version("1.0")
		.author("Nathan Clack <nclack@gmail.com>")
		.subcommand(SubCommand::with_name("list")
			.about("Just list the cards.")
			.arg(Arg::with_name("input")
				.help("The card file to use")
				.required(true)
				)
			)
		.subcommand(SubCommand::with_name("shuffle")
			.about("Shuffle the cards.")
			.arg(Arg::with_name("input")
				.help("The card file to use")
				.required(true)
				)
			)
		.subcommand(SubCommand::with_name("deal")
			.about("Shuffle the cards.")
			.arg(Arg::with_name("input")
				.help("The card file to use")
				.required(true)
				)
			.arg(Arg::with_name("count")
				.help("The number of cards to deal")
				.takes_value(true)
				.default_value("5")
				)
			)
		.get_matches();		
	
	if let Some(matches)=matches.subcommand_matches("list") {
		let cards=read_cards(Path::new(matches.value_of("input").unwrap()))?;
		for card in cards {
			println!("{}",card);
		}
	} else if let Some(matches)=matches.subcommand_matches("shuffle") {
		let mut cards=read_cards(Path::new(matches.value_of("input").unwrap()))?;
		cards.shuffle(&mut thread_rng());
		for card in cards {
			println!("{}",card);
		}
	} else if let Some(matches)=matches.subcommand_matches("deal") {
		let mut cards=read_cards(Path::new(matches.value_of("input").unwrap()))?;
		cards.shuffle(&mut thread_rng());
		let n=matches.value_of("count").unwrap().parse()?;
		let cards:Vec<Card>=cards.iter().take(n).map(|e|e.clone()).collect();
		for card in cards {
			println!("{}",card);
		}
	}
	Ok(())
}