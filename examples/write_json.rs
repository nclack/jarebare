use jarebare::{
    Card,
    Ability,
    Action::{
        Attack,
        Defend
    }
};
use serde_json::Result;


fn main()->Result<()>{
    let cards=[
        Card {
            tier: 1,
            name: "Lil' Goblin".to_string(),
            class: "Goblin".to_string(),
            comment: "Harmless, mostly".to_string(),
            health: 8,
            defense: 1,
            abilities: [
                Ability{ name: "Slash".to_string(), action: Some(Attack("1d6".to_string()))},
                Ability{ name: "Slash".to_string(), action: Some(Attack("1d6".to_string()))},
                Ability{ name: "Flurry".to_string(), action: Some(Attack("2d4".to_string()))},
                Ability{ name: "Flurry".to_string(), action: Some(Attack("2d4".to_string()))},
                Ability{ name: "Block".to_string(), action: Some(Defend("1d4".to_string()))},
                Ability{ name: "Cackle".to_string(), action: None},
            ]

        },
        Card {
            tier: 1,
            name: "Lil' Goblin".to_string(),
            class: "Goblin".to_string(),
            comment: "Harmless, mostly".to_string(),
            health: 8,
            defense: 1,
            abilities: [
                Ability{ name: "Slash".to_string(), action: Some(Attack("1d6".to_string()))},
                Ability{ name: "Slash".to_string(), action: Some(Attack("1d6".to_string()))},
                Ability{ name: "Flurry".to_string(), action: Some(Attack("2d4".to_string()))},
                Ability{ name: "Flurry".to_string(), action: Some(Attack("2d4".to_string()))},
                Ability{ name: "Block".to_string(), action: Some(Defend("1d4".to_string()))},
                Ability{ name: "Cackle".to_string(), action: None},
            ]

        },
    ];
    let s=serde_json::to_string_pretty(&cards)?;
    println!("{}",s);
    Ok(())
}
